# BudgetHub About

## Overview

- [BudgetHub](https://www.budgethub.xyz) is an open source budgeting platform.
- This is the website of BudgetHub.
- A public deployment of this code is maintained by [BudgetHub](https://www.budgethub.xyz).

## Tech Stack

This site was built using [Hugo](https://gohugo.io/).

## How to contribute

- Fork this repository
- Write your awesome feature/fix/refactor
- Create a merge request
- Wait for your MR to be reviewed and merged
- You're now a contributor!

For more information, check out our [guidelines for contributing](CONTRIBUTING.md).

## Quick Start

```shell
git clone https://gitlab.com/budgethub/about
cd about
git submodule init
git submodule update
hugo server
```

Open your browser to http://localhost:1313/ to access the site.
