# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.1.0 - 2018-10-16

### Added

- This project.
- Development tools and environment setup. [!1](https://gitlab.com/budgethub/about/merge_requests/1)
- App releases through Docker. [!2](https://gitlab.com/budgethub/about/merge_requests/2)
- Continuous Deployment to Kubernetes through Gitlab CI. [!3](https://gitlab.com/budgethub/about/merge_requests/3)
