# How To Contribute

We love merge requests from everyone. Reading this document means you're considering to take time out of your busy schedule to contribute to this project and that is most appreciated.

## Important Reminders

- When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.
- By participating in this project, you agree to abide by [our code of conduct](CODE_OF_CONDUCT.md).

## Getting Started

First of all, make sure that you have Hugo installed on your machine.

- [How to install Hugo](https://gohugo.io/getting-started/installing/)

Afterwards, fork this project, then clone your repo:

```shell
git clone git@gitlab.com:your-username/about.git
```

Get the dependencies needed for this project:

```shell
git submodule init
git submodule update
```

Finally, start Hugo's local server so you can run your app in development:

```shell
hugo server
```

The site is located (by default) at http://localhost:1313

## Coding Standards

When contributing, please follow the coding standards mentioned below so we can have nice and consistent-looking code that's easy to read for everyone.

### EditorConfig

Use an editor (or a plugin for your editor) that supports [EditorConfig](http://editorconfig.org).

Our [.editorconfig file](.editorconfig) should set your editor to BudgetHub's preferred settings automatically:

- [UTF-8 charset](https://en.wikipedia.org/wiki/UTF-8)
- [Unix-style line breaks](http://www.cs.toronto.edu/~krueger/csc209h/tut/line-endings.html)
- [End file with a newline](https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline)
- [No trailing whitespace before a line break](https://softwareengineering.stackexchange.com/questions/121555/why-is-trailing-whitespace-a-big-deal)
- Use 2 spaces instead of tabs for indentation

## New to Hugo?

Hugo one of the most popular open-source static site generators.

Here are some resource to help you get started:

### Hugo

- [Official Getting Started Guide](https://gohugo.io/getting-started/)
- [Getting Started Guide by OpenSource.com](https://opensource.com/article/18/3/start-blog-30-minutes-hugo)

### Markdown

- [Mastering Markdown guide by GitHub](https://guides.github.com/features/mastering-markdown/)
- [Markdown syntax documentation by Daring Fireball](https://daringfireball.net/projects/markdown/syntax)
- [Markdown cheatsheet by Adam Pritchard](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
